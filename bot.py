import discord
import os
from markov import generate_monster
from commands.mark import mark
from commands.mark_actions import mark_actions
from commands.mark_bot_info import mark_bot_info
from commands.mark_top_info import mark_top_info
from commands.mark_stats import mark_stats
from commands.mark_specials import mark_specials
from commands.mark_help import mark_help


client = discord.Client()

commands = {
    "mark": mark,
    "mark-actions": mark_actions,
    "mark-specials": mark_specials,
    "mark-bot-info": mark_bot_info,
    "mark-top-info": mark_top_info,
    "mark-stats": mark_stats,
    "mark-help": mark_help
}


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user or not(message.content.startswith('!')):
        return
    args = message.content[1:].rstrip().split()

    rawCommand = args.pop(0)

	#check if rawCommand is undefined
    if (not(rawCommand)):
        return
    commandName = rawCommand.lower()

	#check if have commmand
    if commandName not in commands:
        return

    command = commands[commandName]



    try:
        commandReturn = await command["execute"](message, args, commands)
        if (type(commandReturn) == str):
            return await message.reply(commandReturn + "\nUsage: " + command["usage"])
    except BaseException as err:
        print(repr(err))
        await message.reply("there was an error trying to execute that command!\n")
	

client.run(os.environ.get('DISCORD_TOKEN'))
