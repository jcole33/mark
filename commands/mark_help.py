import sys
from markov import generate_monster
from helper import blockify

async def execute(message, args, commands):
	response = ""
	if not(args):
		response += "Here's a list of all available commands: " + ', '.join(commands.keys()) + "\n"
		response += "You can send '!mark-help <command name>' to get info on a specific command!\n"
		try:
			await message.author.send(blockify(response))
			if message.channel.id == message.author.dm_channel.id:
				return True
			await message.reply("I've sent you a DM with all my commands!")
		except BaseException as err:
			print("Could not send help DM to " + message.author.tag + ".\n" + repr(err), file=sys.stderr)
			await message.reply("It seems like I can't DM you! Do you have DMs disabled?") 
		return True
	commandName = args[0].lower()
	if commandName not in commands:
		return "That's not a valid command!"
	command = commands[commandName]
	response += "Name: " + command["name"] + "\n"
	response += "Description: " + command["description"] + "\n"
	response += "Usage: " + command["usage"] + "\n"
	await message.channel.send(blockify(response))
	return True

mark_help = {
	"name": "mark-help",
	"description": "Lists available commands",
	"usage": "!mark-help or !mark-help <command name>",
	"execute": execute
}


