from markov import specials
from helper import blockify

async def execute(message, args, commands):
	amount = int(args[0]) if args else 1
	await message.channel.send(blockify(specials(amount)))
	return True

mark_specials = {
	"name": "mark-specials",
	"description": "Create a number (default 1) of special abilities",
	"usage": "!mark-specials or !mark-specials <number>",
	"execute": execute
}