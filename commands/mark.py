from markov import generate_monster
from helper import blockify

async def execute(message, args, commands):
    await message.channel.send(blockify(generate_monster()))
    return True


mark = {
	"name": "mark",
	"description": "Create a monster",
	"usage": "!mark",
	"execute": execute
}