from markov import stats
from helper import blockify


async def execute(message, args, commands):
	await message.channel.send(blockify(stats()))
	return True


mark_stats = {
	"name": "mark-stats",
	"description": "Create a monster stats, saving bonuses, and skill bonuses",
	"usage": "!mark-stats",
	"execute": execute
}