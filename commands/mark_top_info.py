from markov import topInfo
from helper import blockify


async def execute(message, args, commands):
	await message.channel.send(blockify(topInfo()))
	return True


mark_top_info = {
	"name": "mark-top-info",
	"description": "Create monster HP, AC, speed, size, type, and alignment",
	"usage": "!mark-top-info",
	"execute": execute
}