import markovify
import random

with open("/usr/src/app/text/botInfo.txt") as f:
    bot_model = markovify.NewlineText(f.read(), 2, None, None, True, False, "None")

with open("/usr/src/app/text/topInfo.txt") as f:
    top_model = markovify.NewlineText(f.read(), 2, None, None, True, False, "None")

with open("/usr/src/app/text/stats.txt") as f:
    stats_model = markovify.NewlineText(f.read(), 3, None, None, True, False, "None")
    
with open("/usr/src/app/text/actions.txt") as f:
    actions_model = markovify.NewlineText(f.read(), 4, None, None, True, False, "None")

with open("/usr/src/app/text/specialAbilities.txt") as f:
    specials_model = markovify.NewlineText(f.read(), 4, None, None, True, False, "None")

def botInfo():
    return str(bot_model.make_short_sentence(500)).replace("|", "\n")

def topInfo():
    return str(top_model.make_short_sentence(100)).replace("|", "\n")

def stats():
    return str(stats_model.make_short_sentence(200)).replace("/|", "\n\n").replace("|", "\n").replace("/", " | ")

def actions(amount): 
    output = ""

    createNum = 0
    while (amount != createNum):
        this_output = actions_model.make_short_sentence(300)
        if (this_output != None):
            createNum += 1
            output += this_output + "\n"
    return str(output).replace("|", "\t\t")

def specials(amount):
    output = ""
    createNum = 0
    while (createNum != amount):
        this_output = specials_model.make_short_sentence(150)
        if (this_output != None):
            createNum += 1
            output += this_output + "\n"
    return  str(output).replace("|", "\n")


def generate_monster():
    output = "Markovian Monster\n"
    output += "---------------------------\n"
    output += str(topInfo()) + "\n\n"
    output += str(stats())
    output += str(botInfo()) + "\n\n"
    output += "Special Abilities\n"
    output += "---------------------------\n"
    output += str(specials(random.choice([1, 2, 3]))) + "\n"
    output += "Actions\n"
    output += "---------------------------\n"
    output += str(actions(random.choice([1, 2, 3])))
    return output



print(specials(10))