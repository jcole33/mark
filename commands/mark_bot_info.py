from markov import botInfo
from helper import blockify

async def execute(message, args, commands):
	await message.channel.send(blockify(botInfo()))
	return True


mark_bot_info = {
	"name": "mark-bot-info",
	"description": "Create monster language, senses, immunities, resistances, and vulnerabilities",
	"usage": "!mark-bot-info",
	"execute": execute
}