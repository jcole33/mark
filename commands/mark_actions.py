from markov import actions
from helper import blockify

async def execute(message, args, commands):
	amount = int(args[0]) if args else 1
	await message.channel.send(blockify(actions(amount)))
	return True

mark_actions = {
	"name": "mark-actions",
	"description": "Create a number (default 1) of actions",
	"usage": "!mark-actions or !mark-actions <number>",
	"execute": execute
}